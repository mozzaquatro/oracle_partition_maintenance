grant select on dba_part_tables to owner;
grant select on dba_tab_partitions to owner;
grant alter any table to owner;
grant drop any table to owner;